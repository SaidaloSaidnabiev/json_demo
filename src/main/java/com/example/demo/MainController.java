package com.example.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.Locale;
import java.util.Map;

@RestController
public class MainController {
    @Autowired
    ObjectMapper objectMapper;

    @GetMapping("/jsonParse")
    public String addSmartphone() {
        File resource = null;
        String reply = "Detected some complications! Please try later";
        try {
            resource = new ClassPathResource("./activate.json").getFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            // create object mapper instance
            ObjectMapper mapper = new ObjectMapper();

            // convert JSON file to map
            Map<?, ?> map = mapper.readValue(resource, Map.class);

            // print map entries
            reply = "";
            for (Map.Entry<?, ?> entry : map.entrySet()) {
                reply += entry.getKey() + "=\n" + entry.getValue();
            }
            reply += map.get("name");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return reply;
    }
}
